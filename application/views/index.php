<?php require_once('template/header.php'); ?>
<div class="container-fluid">
	<div class="row">
		<div class="jumbotron landing">
			<div class="container">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<p>Pusat Informasi Kota Palembang, merupakan layanan informasi yang dapat dimanfaatkan dan diakses secara langsung oleh masyarakat. Mulai dari perizinan, hingga informasi mengenai ketersediaan kamar di RS</p>
					<a href="<?php echo base_url('kategori/0/0'); ?>" class="btn btn-custom-green btn-lg">Lihat Layanan</a>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 cs">
					<img src="<?php echo base_url('assets'); ?>/img/customer-service-1.png" class="img-responsive" alt="Image">
				</div>
			</div>
		</div>
	</div>
</div>
<!-- landing 1 -->
<div class="mt50"></div>
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
			<h1 class="heading_service">Pelayanan Online</h1>
			<div class="divier_heading"></div>
			<h4>Pelayanan online meliputi perizinan dan sejenisnya</h4>
			<p>Dinas pendidikan, BPPD, RSUD Bari, Kecamatan, Dinas Kesehatan, Dinas Perhubungan, Dinas Kebakaran, PTSP, dan Dinas Kependudukan & Catatan Sipil</p>
			<div class="mt20"></div>
			<a href="<?= base_url('Kategori/0/0/') ?>"><button class="btn btn-custom-blue"><h4>Layanan Kami</h4></button></a>
			<div class="mt10"></div>
			<a href="<?= base_url('Jadwal') ?>"><button class="btn btn-custom-blue"><h4>Jadwal Walikota</h4></button></a>
			<div class="mt10"></div>
			<a href="<?= base_url('') ?>"><button class="btn btn-custom-blue"><h4>Laporan warga</h4></button></a>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 landing_dinas_list">
			<?php foreach($kategori as $kategori):  ?>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="media">
					<div class="media-left">
					<a href="<?= base_url() ?>kategori/<?= $kategori->kd_kategori ?>/<?= $kategori->kd_kategori ?>">
						<img src="<?= base_url('assets') ?>/img/pemkot.png" class="media-object" style="width:60px">
					</a>
					</div>
					<div class="media-body">
						<h4 class="media-heading">
							<a href="<?= base_url() ?>kategori/<?= $kategori->kd_kategori ?>/<?= $kategori->kd_kategori ?>"> 
								<?= $kategori->nama_kategori ?> 
							</a></h4>
						<p><span><?= $kategori->info_tambahan ?></p>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>
<!-- landing 2 -->
<div class="mt50"></div>
<div class="container-fluid landing2_bg">
	<div class="row">
		<div class="container">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<img src="<?php echo base_url('assets'); ?>/img/landing2.png" class="img-responsive" alt="Image">
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<h1>Profil Pusat Informasi</h1>
				<div class="divier_heading"></div>
				<p>Pusat Informasi Kota Palembang ditujukan untuk menyempurnakan pelayanan publik dari Pemerintah Kota Palembang kepada masyarakat dan menjadi salah satu inovasi Pemerintah Kota Palembang dalam hal pelayanan publik serta diharapkan menjadi salah satu kunci penting dalam upaya Pemerintah Kota Palembang untuk mewujudkan Palembang Smart City</p>
				<button class="btn btn-custom-green"><h4>Profil Kami</h4></button>
			</div>
		</div>
	</div>
</div>
<!-- landing 3 -->
<div class="mt50"></div>
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
			<h1 class="heading_service">FAQ</h1>
			<div class="divier_heading"></div>
			<h4>Pertanyaan-pertanyaan yang sering diajukan</h4>
			<p>
				Temukan jawaban dari hal-hal yang paling sering ditanyakan mengenai Pusat Informasi Kota Palembang. Jika tidak menemukan jawaban yang Anda cari, silakan klik tombol dibawah:
			</p>
			<div class="mt20"></div>
			<button class="btn btn-custom-blue"><h4>Hubungi Kami</h4></button>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
			<?php for($i=1; $i<=6; $i++): ?>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="media">
					<div class="media-body">
						<h4>This is Heading</h4>
						<p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque voluptatem, amet, qui quas fuga nostrum quae vel iusto modi voluptatibus aspernatur. Illo dicta sequi laboriosam iusto, consequatur sunt quae sapiente</p>
					</div>
				</div>
			</div>
			<?php endfor; ?>
		</div>
	</div>
</div>