<?php require_once('template/header.php'); ?>
<div class="container-fluid">
	<div class="row">
		<div class="jumbotron layanan">
			<div class="container">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h2 class="text-center">Jadwal Walikota</h2>
					<p class="text-center">Berikut ini adalah jadwal kunjungan walikota</p>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- landing 1 -->
<div class="mt50"></div>
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<?php foreach($jadwal as $jadwal):  ?>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="container_shadow">
					<div class="media">
						<div class="media-left">
							<i class="fa fa-calendar fa-3x"></i>
						</div>
						<div class="media-body">
							<h4 class="media-heading">Jadwal Walikota</h4>
							<p><span><?= $jadwal->tanggal ?></p>
							<a href="<?= base_url(); ?>assets/img/<?= $jadwal->isi ?>" target = "_blank">
								<button type="button" class="btn btn-custom-green">Lihat Jadwal</button></a>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>