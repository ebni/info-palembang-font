<?php require_once('template/header.php'); ?>
<div class="container-fluid">
	<div class="row">
		<div class="jumbotron layanan">
			<div class="container">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h2 class="text-center">Layanan Pilihan</h2>
					<p class="text-center">Berikut adalah berbagai informasi setiap dinas maupun badan pemerintahan beserta layanannya</p>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- landing 1 -->
<div class="mt50"></div>
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<?php foreach($kategori as $kategori): ?>
			<div class="col-sm-3 col-md-3">
				<div class="thumbnail">
					<p class="text-center">
						<?php if($kd_parent == '0'){ ?>
							<a href = "<?= base_url() ?>kategori/<?= $kategori->kd_kategori ?>/<?php if($kd_parent == 0){ echo $kategori->kd_kategori; } else{ echo $kd_parent; } ?>" >
								<img src="<?= base_url('assets'); ?>/img/<?= $kategori->logo ?>" class="img-responsive">
							</a>
						<?php }else{ ?>
							<i class="<?= $kategori->logo ?>" aria-hidden="true"></i>
						<?php } ?>
					</p>
				<div class="caption text-justify">
					<h3 class="text-center"><?= $kategori->nama_kategori ?></h3>
					<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque voluptatum velit expedita dolorem dolores voluptatibus.</p> -->

					<p class="text-center"><a href="<?= base_url() ?>kategori/<?= $kategori->kd_kategori ?>/<?php if($kd_parent == 0){ echo $kategori->kd_kategori; } else{ echo $kd_parent; } ?>" class="btn btn-custom-green" role="button">Lihat Layanan <i class="fa fa-arrow-right"></i></a></p>
				</div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>