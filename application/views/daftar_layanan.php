<?php require_once('template/header.php'); ?>
<div class="container-fluid">
	<div class="row">
		<div class="jumbotron layanan">
			<div class="container">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<img src="img/pemkot.png" class="img-responsive logo_dinas">
					<p class="text-center">Daftar Layanan Pada Dinas Komunikasi dan Informatika Kota Palembang</p>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- landing 1 -->
<div class="mt50"></div>
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="panel panel-default panel_custom">
				<div class="panel-body">
					<h3 class="text-heading"><?= strtoupper($nama_kategori) ?></h3>
					<p><span><?= $info_tambahan ?></span></p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<?php foreach($layanan as $layanan):  ?>
			<h3 class="et_pb_module_header"><?= $layanan->nama_kategori ?></h3>

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingOne">
						<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse" aria-expanded="true" aria-controls="collapse">
						Deskripsi Layanan <span><i class="fa fa-arrow-circle-down" aria-hidden="true"></i></span>
						</a>
						</h4>
					</div>
					<div id="collapse<?= $i ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
						<div class="panel-body">
						<?= $layanan->deskripsi ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingOne">
						<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse" aria-expanded="true" aria-controls="collapse">
						Persyaratan <span><i class="fa fa-arrow-circle-down" aria-hidden="true"></i></span>
						</a>
						</h4>
					</div>
					<div id="collapse<?= $i ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
						<div class="panel-body">
						<?= $layanan->syarat ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingOne">
						<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse" aria-expanded="true" aria-controls="collapse">
						Persyaratan <span><i class="fa fa-arrow-circle-down" aria-hidden="true"></i></span>
						</a>
						</h4>
					</div>
					<div id="collapse<?= $i ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
						<div class="panel-body">
						<?= $layanan->syarat ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingOne">
						<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse" aria-expanded="true" aria-controls="collapse">
						Prosedur <span><i class="fa fa-arrow-circle-down" aria-hidden="true"></i></span>
						</a>
						</h4>
					</div>
					<div id="collapse<?= $i ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
						<div class="panel-body">
						<?= $layanan->prosedur ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingOne">
						<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse" aria-expanded="true" aria-controls="collapse">
						Biaya <span><i class="fa fa-arrow-circle-down" aria-hidden="true"></i></span>
						</a>
						</h4>
					</div>
					<div id="collapse<?= $i ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
						<div class="panel-body">
						<?= $layanan->biaya ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingOne">
						<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse" aria-expanded="true" aria-controls="collapse">
						Kontak <span><i class="fa fa-arrow-circle-down" aria-hidden="true"></i></span>
						</a>
						</h4>
					</div>
					<div id="collapse<?= $i ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
						<div class="panel-body">
						<p>Email: <a href="mailto:<?= $email ?>"><?= $email ?></a></p>
					<p>Telpon: <?= $kontak ?></p>
					<a href="https://api.whatsapp.com/send?phone=<?= $kontak ?>&text=Apa%20yang%20dapat%20kami%20bantu?%20Silahkan%20chat%20disini"><p><img src="<?= base_url() ?>assets/konten/unggah/2018/09/whatsapp.png" width="300px"></p></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingOne">
						<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse" aria-expanded="true" aria-controls="collapse">
						Lama Penyelesaian <span><i class="fa fa-arrow-circle-down" aria-hidden="true"></i></span>
						</a>
						</h4>
					</div>
					<div id="collapse<?= $i ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
						<div class="panel-body">
						<?= $layanan->lama_penyelesaian ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingOne">
						<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse" aria-expanded="true" aria-controls="collapse">
						Waktu Layanan <span><i class="fa fa-arrow-circle-down" aria-hidden="true"></i></span>
						</a>
						</h4>
					</div>
					<div id="collapse<?= $i ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
						<div class="panel-body">
						<?= $layanan->jam_layanan ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingOne">
						<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse" aria-expanded="true" aria-controls="collapse">
						Info Tambahan <span><i class="fa fa-arrow-circle-down" aria-hidden="true"></i></span>
						</a>
						</h4>
					</div>
					<div id="collapse<?= $i ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
						<div class="panel-body">
						<?= $layanan->info_tambahan ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingOne">
						<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse" aria-expanded="true" aria-controls="collapse">
						Alamat <span><i class="fa fa-arrow-circle-down" aria-hidden="true"></i></span>
						</a>
						</h4>
					</div>
					<div id="collapse<?= $i ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
						<div class="panel-body">
						<?= $alamat ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingOne">
						<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse" aria-expanded="true" aria-controls="collapse">
						Lokasi <span><i class="fa fa-arrow-circle-down" aria-hidden="true"></i></span>
						</a>
						</h4>
					</div>
					<div id="collapse<?= $i ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
						<div class="panel-body">
						
				<div class="et_pb_toggle_content clearfix">
					<p><?= $lokasi ?></p>
					<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 400px;">
							<div class="height-sm"  >
								<ul class="media-list media-list-with-divider" style="height:  120px;">
								
								<?php echo $map['js'];?>
								<?php echo $map['html'];?>
 
								</ul>
							</div>
							</div>
				</div> <!-- .et_pb_toggle_content -->
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php endforeach; ?>
	</div>
</div>