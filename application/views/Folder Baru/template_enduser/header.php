<!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<script type="text/javascript">
			document.documentElement.className = 'js';
		</script>
		
		<title>Pusat Informasi Kota Palembang</title>
		
		<link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
		<link rel='dns-prefetch' href='http://s.w.org/' />
		<meta content="Divi v.3.15" name="generator"/>
		<link rel='stylesheet' id='wtfdivi-user-css-css'  href='<?php echo base_url(); ?>assets/konten/unggah/wtfdivi/wp_head5010.css?ver=4.9.8' type='text/css' media='all' />
		<link rel='stylesheet' id='divi-fonts-css'  href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&amp;subset=latin,latin-ext' type='text/css' media='all' />
		<link rel='stylesheet' id='divi-style-css'  href='<?php echo base_url(); ?>assets/konten/tema/Maulana/style90d0.css?ver=3.15' type='text/css' media='all' />
		<link rel='stylesheet' id='dashicons-css'  href='<?php echo base_url(); ?>assets/rangkuman/css/dashicons.min5010.css?ver=4.9.8' type='text/css' media='all' />
		<script type='text/javascript' src='<?php echo base_url(); ?>assets/rangkuman/js/jquery/jqueryb8ff.js?ver=1.12.4'></script>
		<script type='text/javascript' src='<?php echo base_url(); ?>assets/rangkuman/js/jquery/jquery-migrate.min330a.js?ver=1.4.1'></script>
		<link rel="canonical" href="<?php echo base_url(); ?>" />
		<style>
			.db_pb_team_member_website_icon:before{content:"\e0e3";}
			.db_pb_team_member_email_icon:before{content:"\e010";}
			.db_pb_team_member_instagram_icon:before{content:"\e09a";}
		</style>
		<style>#et_builder_outer_content .db_pb_button_2,.db_pb_button_2{margin-left:30px}</style>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
		<link rel="stylesheet" id="et-core-unified-cached-inline-styles" href="<?php echo base_url(); ?>assets/konten/cache/et/2/et-core-unified-15381462951132.min.css" onerror="et_core_page_resource_fallback(this, true)" onload="et_core_page_resource_fallback(this)" />
	</head>
	<body class="page-template-default page page-id-2 et_pb_button_helper_class et_fixed_nav et_show_nav et_cover_background et_pb_gutter windows et_pb_gutters3 et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_pb_footer_columns3 et_header_style_left et_pb_pagebuilder_layout et_right_sidebar et_divi_theme et-db et_minified_js et_minified_css">
		<div id="page-container">
			<header id="main-header" data-height-onload="66">
				<div class="container clearfix et_menu_container">
					<div class="logo_container">
						<span class="logo_helper"></span>
						<a href="<?= base_url() ?>">
							<img src="<?php echo base_url(); ?>assets/konten/unggah/2018/09/logo-1.png" alt="tommy-maulana" id="logo" style="max-height: 50px !important;" />
						</a>
					</div>
					<div id="et-top-navigation" data-height="66" data-fixed-height="66">
						<nav id="top-menu-nav">
							<ul id="top-menu" class="nav">
								<li id="menu-item-273" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-273"><a href="<?php echo base_url(); ?>">Home</a></li>
								<li id="menu-item-273" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-273"><a href="<?= base_url() ?>kategori/0/0">Layanan</a></li>
								<li id="menu-item-271" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-271"><a href="#">Profil Kami</a></li>
								<li id="menu-item-272" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-272"><a href="#">Hubungi Kami</a></li>
							</ul>						
						</nav>
						<div id="et_mobile_nav_menu">
							<div class="mobile_nav closed">
								<span class="select_page">Select Page</span>
								<span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
							</div>
						</div>				
					</div> <!-- #et-top-navigation -->
				</div> <!-- .container -->
				<div class="et_search_outer">
					<div class="container et_search_form_container">
						<form role="search" method="get" class="et-search-form" action="#">
							<input type="search" class="et-search-field" placeholder="Search &hellip;" value="" name="s" title="Search for:" />					
						</form>
						<span class="et_close_search_field"></span>
					</div>
				</div>
			</header> <!-- #main-header -->