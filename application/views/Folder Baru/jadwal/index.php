<!DOCTYPE html>
<html lang="en-US">
	<!-- Added by tmy --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by tmy -->
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<script type="text/javascript">
			document.documentElement.className = 'js';
		</script>
		<title>Pusat Informasi Kota Palembang</title>
		<link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
		<link rel='dns-prefetch' href='http://s.w.org/' />
		<meta content="Divi v.3.15" name="generator"/>
		<style type="text/css">
			img.wp-smiley,
			img.emoji {
				display: inline !important;
				border: none !important;
				box-shadow: none !important;
				height: 1em !important;
				width: 1em !important;
				margin: 0 .07em !important;
				vertical-align: -0.1em !important;
				background: none !important;
				padding: 0 !important;
			}
		</style>
		<link rel='stylesheet' id='wtfdivi-user-css-css'  href='<?php echo base_url(); ?>assets/content/uploads/wtfdivi/wp_head5010.css?ver=4.9.8' type='text/css' media='all' />
		<link rel='stylesheet' id='divi-fonts-css'  href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&amp;subset=latin,latin-ext' type='text/css' media='all' />
		<link rel='stylesheet' id='divi-style-css'  href='<?php echo base_url(); ?>assets/content/themes/Divi/style90d0.css?ver=3.15' type='text/css' media='all' />
		<link rel='stylesheet' id='et-builder-googlefonts-cached-css'  href='http://fonts.googleapis.com/css?family=Rubik%3A300%2C300italic%2Cregular%2Citalic%2C500%2C500italic%2C700%2C700italic%2C900%2C900italic&amp;ver=4.9.8#038;subset=cyrillic,hebrew,latin,latin-ext' type='text/css' media='all' />
		<link rel='stylesheet' id='dashicons-css'  href='<?php echo base_url(); ?>assets/inklut/css/dashicons.min5010.css?ver=4.9.8' type='text/css' media='all' />
		<script type='text/javascript' src='<?php echo base_url(); ?>assets/inklut/js/jquery/jqueryb8ff.js?ver=1.12.4'></script>
		<script type='text/javascript' src='<?php echo base_url(); ?>assets/inklut/js/jquery/jquery-migrate.min330a.js?ver=1.4.1'></script>
		<meta name="generator" content="WordPress 4.9.8" />
		<link rel="canonical" href="<?php echo base_url(); ?>" />
		<link rel='shortlink' href='<?php echo base_url(); ?>' />
		<style>
			.db_pb_team_member_website_icon:before{content:"\e0e3";}
			.db_pb_team_member_email_icon:before{content:"\e010";}
			.db_pb_team_member_instagram_icon:before{content:"\e09a";}
		</style>
		<style>#et_builder_outer_content .db_pb_button_2,.db_pb_button_2{margin-left:30px}</style>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
		<link rel="stylesheet" id="et-core-unified-cached-inline-styles" href="<?php echo base_url(); ?>assets/content/cache/et/307/et-core-unified-15381818803064.min.css" onerror="et_core_page_resource_fallback(this, true)" onload="et_core_page_resource_fallback(this)" />
	</head>
	<body class="page-template-default page page-id-307 et_pb_button_helper_class et_fixed_nav et_show_nav et_cover_background et_pb_gutter windows et_pb_gutters3 et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_pb_footer_columns3 et_header_style_left et_pb_pagebuilder_layout et_right_sidebar et_divi_theme et-db et_minified_js et_minified_css">
		<div id="page-container">	
			<header id="main-header" data-height-onload="66">
				<div class="container clearfix et_menu_container">
					<div class="logo_container">
						<span class="logo_helper"></span>
							<a href="<?php echo base_url(); ?>">
								<img src="<?php echo base_url(); ?>assets/content/uploads/2018/09/logo-1.png" alt="tommy-maulana" id="logo" data-height-percentage="83" />
							</a>
					</div>
					<div id="et-top-navigation" data-height="66" data-fixed-height="66">
						<nav id="top-menu-nav">
							<ul id="top-menu" class="nav">
								<li id="menu-item-273" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-273"><a href="<?php echo base_url(); ?>">Home</a></li>
								<li id="menu-item-273" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-273"><a href="<?= base_url() ?>kategori/0/0">Layanan</a></li>
								<li id="menu-item-271" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-271"><a href="#">Profil Kami</a></li>
								<li id="menu-item-272" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-272"><a href="#">Hubungi Kami</a></li>
							</ul>						
						</nav>
						<div id="et_mobile_nav_menu">
							<div class="mobile_nav closed">
								<span class="select_page">Select Page</span>
								<span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
							</div>
						</div>				
					</div> <!-- #et-top-navigation -->
				</div> <!-- .container -->
				<div class="et_search_outer">
					<div class="container et_search_form_container">
						<form role="search" method="get" class="et-search-form" action="#">
							<input type="search" class="et-search-field" placeholder="Search &hellip;" value="" name="s" title="Search for:" />					
						</form>
						<span class="et_close_search_field"></span>
					</div>
				</div>
			</header> <!-- #main-header -->
			
			<div id="et-main-area">	
				<div id="main-content">
					<article id="post-307" class="post-307 page type-page status-publish hentry">
						<div class="entry-content">
							<div id="et-boc" class="et-boc">
								<div class="et_builder_inner_content et_pb_gutters3">
									<div class="et_pb_section et_pb_section_0 et_pb_with_background et_pb_section_parallax et_pb_fullwidth_section et_section_regular section_has_divider et_pb_top_divider">
										<div class="et_pb_top_inside_divider"></div>
										<div class="et_parallax_bg" style="background-image: url(<?php echo base_url(); ?>assets/content/uploads/2018/09/hdgdgh.jpg);"></div>
										<section class="et_pb_module et_pb_fullwidth_header et_pb_fullwidth_header_0 et_animated et_pb_bg_layout_light et_pb_text_align_center">
											<div class="et_pb_fullwidth_header_container center">
												<div class="header-content-container center">
													<div class="header-content">
														<span class="et_pb_fullwidth_header_subhead">Jadwal Walikota</span>
														<div class="et_pb_header_content_wrapper"></div>
													</div>
												</div>
											</div>
											<div class="et_pb_fullwidth_header_overlay"></div>
											<div class="et_pb_fullwidth_header_scroll"></div>
										</section>
									</div> <!-- .et_pb_section -->
									<div class="et_pb_section et_pb_section_1 et_section_regular">
										<div class="et_pb_row et_pb_row_0">
										<?php 
										foreach($jadwal as $jadwal): 
										?>	
											<div class="et_pb_column et_pb_column_1_2 et_pb_column_0    et_pb_css_mix_blend_mode_passthrough">
												<div class="et_pb_with_border et_pb_module et_pb_blurb et_pb_blurb_0 et_animated et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_left">
													<div class="et_pb_blurb_content">
														<div class="et_pb_main_blurb_image">
															<span class="et_pb_image_wrap">
																<span class="et-pb-icon et-waypoint et_pb_animation_off" style="color: #ff7355;">&#x79;</span>
															</span>
														</div>
														<div class="et_pb_blurb_container">
															<a href="<?= base_url(); ?>assets/img/<?= $jadwal->isi ?>" target = "_blank"><h4 class="et_pb_module_header"><b>Download <?= $jadwal->isi ?></b></h4></a>
															<div class="et_pb_blurb_description">
																<p><?= $jadwal->tanggal ?></p>
															</div><!-- .et_pb_blurb_description -->
														</div>
													</div> <!-- .et_pb_blurb_content -->
												</div> <!-- .et_pb_blurb -->
											</div> <!-- .et_pb_column -->
										<?php endforeach ?>
										</div> <!-- .et_pb_row -->
									</div> <!-- .et_pb_section -->			
								</div>			
							</div>					
						</div> <!-- .entry-content -->
					</article> <!-- .et_pb_post -->
				</div> <!-- #main-content -->
				<footer id="main-footer">					
					<div id="footer-bottom">
						<div class="container clearfix">
						</div>	<!-- .container -->
					</div>
				</footer> <!-- #main-footer -->
			</div> <!-- #et-main-area -->
		</div> <!-- #page-container -->
		<script type="text/javascript">
			var et_animation_data = [{"class":"et_pb_fullwidth_header_0","style":"slideBottom","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"1%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_blurb_0","style":"zoom","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"10%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_blurb_1","style":"zoom","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"10%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_blurb_2","style":"zoom","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"10%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_blurb_3","style":"zoom","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"10%","starting_opacity":"0%","speed_curve":"ease-in-out"}];
		</script>
		<script type='text/javascript'>
			/* <![CDATA[ */
			var DIVI = {"item_count":"%d Item","items_count":"%d Items"};
			var et_shortcodes_strings = {"previous":"Previous","next":"Next"};
			var et_pb_custom = {"ajaxurl":"http:\/\/tommy-maulana.com\/wp-admin\/admin-ajax.php","images_uri":"http:\/\/tommy-maulana.com\/content\/themes\/Divi\/images","builder_images_uri":"http:\/\/tommy-maulana.com\/content\/themes\/Divi\/includes\/builder\/images","et_frontend_nonce":"617fe5d187","subscription_failed":"Please, check the fields below to make sure you entered the correct information.","et_ab_log_nonce":"6b9fad679e","fill_message":"Please, fill in the following fields:","contact_error_message":"Please, fix the following errors:","invalid":"Invalid email","captcha":"Captcha","prev":"Prev","previous":"Previous","next":"Next","wrong_captcha":"You entered the wrong number in captcha.","is_builder_plugin_used":"","ignore_waypoints":"no","is_divi_theme_used":"1","widget_search_selector":".widget_search","is_ab_testing_active":"","page_id":"307","unique_test_id":"","ab_bounce_rate":"5","is_cache_plugin_active":"no","is_shortcode_tracking":""};
			var et_pb_box_shadow_elements = [];
			/* ]]> */
		</script>
		<script type='text/javascript' src='<?php echo base_url(); ?>assets/content/themes/Divi/js/custom.min90d0.js?ver=3.15'></script>
		<script type='text/javascript' src='<?php echo base_url(); ?>assets/content/themes/Divi/core/admin/js/common90d0.js?ver=3.15'></script>
		<script type='text/javascript' src='<?php echo base_url(); ?>assets/content/uploads/wtfdivi/wp_footer5010.js?ver=4.9.8'></script>
		<script type='text/javascript' src='<?php echo base_url(); ?>assets/inklut/js/wp-embed.min5010.js?ver=4.9.8'></script>
	</body>
</html>