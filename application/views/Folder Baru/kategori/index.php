<?php $this->load->view('template_enduser/header'); ?>
<div id="main-content">
	<article id="post-181" class="post-181 page type-page status-publish hentry">
		<div class="entry-content">
			<div id="et-boc" class="et-boc">
				<div class="et_builder_inner_content et_pb_gutters3">
					<div class="et_pb_section et_pb_section_0 et_section_regular" style="background-image: url(<?= base_url().'assets/img/list_kategori.png'; ?>) !important;">
						<div class="et_pb_row et_pb_row_0">
							<div class="et_pb_column et_pb_column_4_4 et_pb_column_0 et_pb_css_mix_blend_mode_passthrough et-last-child">
								<div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_light et_pb_text_align_left">
									<div class="et_pb_text_inner">
										<h1 style="color: #FFF; text-align: center;"><strong>Layanan Pilihan</strong></h1>
										<p style="color: #FFF; text-align: center;">Berikut adalah berbagai informasi setiap dinas maupun badan pemerintahan beserta layanannya</p>
									</div>
								</div> <!-- .et_pb_text -->
							</div> <!-- .et_pb_column -->
						</div> <!-- .et_pb_row -->
					</div> <!-- .et_pb_section -->
					<div class="et_pb_section et_pb_section_1 et_pb_with_background et_section_regular">
						<div class="et_pb_row et_pb_row_1">
						<?php 
						foreach($kategori as $kategori): 
						?>	
							<div class="et_pb_column et_pb_column_1_5 et_pb_column_1    et_pb_css_mix_blend_mode_passthrough">
								<div class="et_pb_module et_pb_blurb et_pb_blurb_0 et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_top">
									<div class="et_pb_blurb_content">
										<div class="et_pb_main_blurb_image">
											<span class="et_pb_image_wrap">
												<a href = "<?= base_url() ?>kategori/<?= $kategori->kd_kategori ?>/<?php if($kd_parent == 0){ echo $kategori->kd_kategori; } else{ echo $kd_parent; } ?>" >
													<img src="<?= base_url(); ?>assets/img/<?= $kategori->logo ?>" alt="" class="et-waypoint et_pb_animation_top" />
												</a>
											</span>
										</div>
										<div class="et_pb_blurb_container">
											<div class="et_pb_blurb_description">
											</div><!-- .et_pb_blurb_description -->
										</div>
									</div> <!-- .et_pb_blurb_content -->
								</div> <!-- .et_pb_blurb -->
								<div class="et_pb_button_module_wrapper et_pb_button_0_wrapper et_pb_button_alignment_center et_pb_module ">
									<a class="et_pb_button et_pb_button_0 et_pb_bg_layout_light" href="<?= base_url() ?>kategori/<?= $kategori->kd_kategori ?>/<?php if($kd_parent == 0){ echo $kategori->kd_kategori; } else{ echo $kd_parent; } ?>">
										<?= $kategori->nama_kategori ?>
									</a>
								</div>
							</div> <!-- .et_pb_column -->
						<?php endforeach ?>
						</div>
					</div>
				</div>			
			</div>					
		</div>
	</article>
</div>
<?php $this->load->view('template_enduser/footer'); ?>		