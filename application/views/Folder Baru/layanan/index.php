<!DOCTYPE html>
<html lang="en-US">
	<!-- Added by tmy --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by tmy -->
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<script type="text/javascript">
			document.documentElement.className = 'js';
		</script>
	
		<title>Pusat Informasi Kota Palembang</title>
		
		<link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
		<link rel='dns-prefetch' href='http://s.w.org/' />
		<meta content="Divi v.3.15" name="generator"/>
		<style type="text/css">
			img.wp-smiley,
			img.emoji {
				display: inline !important;
				border: none !important;
				box-shadow: none !important;
				height: 1em !important;
				width: 1em !important;
				margin: 0 .07em !important;
				vertical-align: -0.1em !important;
				background: none !important;
				padding: 0 !important;
			}
		</style>
		<link rel='stylesheet' id='ccw_main_css-css' href='<?php echo base_url(); ?>assets/konten/plugins/click-to-chat-for-whatsapp/assets/css/mainstylesf24c.css?ver=1.6' type='text/css' media='all' />
		<link rel='stylesheet' id='wtfdivi-user-css-css' href='<?php echo base_url(); ?>assets/konten/unggah/wtfdivi/wp_head5010.css?ver=4.9.8' type='text/css' media='all' />
		<link rel='stylesheet' id='divi-fonts-css' href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&amp;subset=latin,latin-ext' type='text/css' media='all' />
		<link rel='stylesheet' id='divi-style-css' href='<?php echo base_url(); ?>assets/konten/amet/Maul/style90d0.css?ver=3.15' type='text/css' media='all' />
		<link rel='stylesheet' id='et-builder-googlefonts-cached-css'  href='http://fonts.googleapis.com/css?family=Playfair+Display%3Aregular%2Citalic%2C700%2C700italic%2C900%2C900italic%7CRubik%3A300%2C300italic%2Cregular%2Citalic%2C500%2C500italic%2C700%2C700italic%2C900%2C900italic&amp;ver=4.9.8#038;subset=cyrillic,latin,latin-ext,hebrew' type='text/css' media='all' />
		<link rel='stylesheet' id='dashicons-css'  href='<?php echo base_url(); ?>assets/include/css/dashicons.min5010.css?ver=4.9.8' type='text/css' media='all' />
		<script type='text/javascript' src='<?php echo base_url(); ?>assets/include/js/jquery/jqueryb8ff.js?ver=1.12.4'></script>
		<script type='text/javascript' src='<?php echo base_url(); ?>assets/include/js/jquery/jquery-migrate.min330a.js?ver=1.4.1'></script>
		<meta name="generator" content="WordPress 4.9.8" />
		<link rel="canonical" href="index.html" />
<style>
.db_pb_team_member_website_icon:before{content:"\e0e3";}
.db_pb_team_member_email_icon:before{content:"\e010";}
.db_pb_team_member_instagram_icon:before{content:"\e09a";}
</style>
<style>#et_builder_outer_content .db_pb_button_2,.db_pb_button_2{margin-left:30px}</style>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<style id="et-core-unified-cached-inline-styles">
	#main-header,#main-header .nav li ul,.et-search-form,#main-header .et_mobile_menu{background-color:#09e1c0}
	#main-header .nav li ul{background-color:#ffffff}
	.nav li ul{border-color:#ffffff}
	.et_header_style_centered .mobile_nav .select_page,.et_header_style_split .mobile_nav .select_page,.et_nav_text_color_light #top-menu>li>a,.et_nav_text_color_dark #top-menu>li>a,#top-menu a,.et_mobile_menu li a,.et_nav_text_color_light .et_mobile_menu li a,.et_nav_text_color_dark .et_mobile_menu li a,#et_search_icon:before,.et_search_form_container input,span.et_close_search_field:after,#et-top-navigation .et-cart-info{color:#ffffff}.et_search_form_container input::-moz-placeholder{color:#ffffff}.et_search_form_container input::-webkit-input-placeholder{color:#ffffff}
	.et_search_form_container input:-ms-input-placeholder{color:#ffffff}
	#main-header .nav li ul a{color:rgba(0,0,0,0)}
	#top-menu li a,.et_search_form_container input{letter-spacing:2px}
	.et_search_form_container input::-moz-placeholder{letter-spacing:2px}
	.et_search_form_container input::-webkit-input-placeholder{letter-spacing:2px}
	.et_search_form_container input:-ms-input-placeholder{letter-spacing:2px}
	#footer-widgets .footer-widget a,#footer-widgets .footer-widget li a,#footer-widgets .footer-widget li a:hover{color:#ffffff}
	.footer-widget{color:#ffffff}
	@media only screen and (min-width:981px){#logo{max-height:83%}
	.et_pb_svg_logo #logo{height:83%}
	.et_header_style_centered.et_hide_primary_logo #main-header:not(.et-fixed-header) 
	.logo_container,.et_header_style_centered.et_hide_fixed_logo #main-header.et-fixed-header .logo_container{height:11.88px}
	.et_header_style_left .et-fixed-header #et-top-navigation,.et_header_style_split .et-fixed-header #et-top-navigation{padding:33px 0 0 0}
	.et_header_style_left .et-fixed-header #et-top-navigation nav>ul>li>a,.et_header_style_split .et-fixed-header #et-top-navigation nav>ul>li>a{padding-bottom:33px}
	.et_header_style_centered header#main-header.et-fixed-header .logo_container{height:66px}
	.et_header_style_split .et-fixed-header .centered-inline-logo-wrap{width:66px;margin:-66px 0}
	.et_header_style_split .et-fixed-header .centered-inline-logo-wrap #logo{max-height:66px}
	.et_pb_svg_logo.et_header_style_split .et-fixed-header .centered-inline-logo-wrap #logo{height:66px}
	.et_header_style_slide .et-fixed-header #et-top-navigation,.et_header_style_fullscreen .et-fixed-header #et-top-navigation{padding:24px 0 24px 0!important}
	.et-fixed-header #top-menu a,.et-fixed-header #et_search_icon:before,.et-fixed-header #et_top_search .et-search-form input,.et-fixed-header .et_search_form_container input,.et-fixed-header .et_close_search_field:after,.et-fixed-header #et-top-navigation .et-cart-info{color:#ffffff!important}
	.et-fixed-header .et_search_form_container input::-moz-placeholder{color:#ffffff!important}
	.et-fixed-header .et_search_form_container input::-webkit-input-placeholder{color:#ffffff!important}
	.et-fixed-header .et_search_form_container input:-ms-input-placeholder{color:#ffffff!important}}
	@media only screen and (min-width:1350px){.et_pb_row{padding:27px 0}.et_pb_section{padding:54px 0}
	.single.et_pb_pagebuilder_layout.et_full_width_page .et_post_meta_wrapper{padding-top:81px}
	.et_pb_section.et_pb_section_first{padding-top:inherit}.et_pb_fullwidth_section{padding:0}}
	#main-footer{display:none}
</style>
</head>
<body class="page-template-default page page-id-268 et_pb_button_helper_class et_fixed_nav et_show_nav et_cover_background et_pb_gutter windows et_pb_gutters3 et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_pb_footer_columns4 et_header_style_left et_pb_pagebuilder_layout et_right_sidebar et_divi_theme et-db et_minified_js et_minified_css">
	<div id="page-container">
			<header id="main-header" data-height-onload="66">
			<div class="container clearfix et_menu_container">
							<div class="logo_container">
					<span class="logo_helper"></span>
					<a href="http://tommy-maulana.com/">
						<img src="<?php echo base_url(); ?>assets/konten/unggah/2018/09/logo-1.png" alt="tommy-maulana" id="logo" data-height-percentage="83" />
					</a>
				</div>
				<div id="et-top-navigation" data-height="66" data-fixed-height="66">
					<nav id="top-menu-nav">
						<ul id="top-menu" class="nav">
								<li id="menu-item-273" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-273"><a href="<?php echo base_url(); ?>">Home</a></li>
								<li id="menu-item-273" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-273"><a href="<?= base_url() ?>kategori/0/0">Layanan</a></li>
								<li id="menu-item-271" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-271"><a href="#">Profil Kami</a></li>
								<li id="menu-item-272" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-272"><a href="#">Hubungi Kami</a></li>
							<li id="menu-item-274" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-274">
								<a href="#">								
									<form role="search" method="get" id="searchform" class="searchform" action="http://tommy-maulana.com/"> 				
										<div> 					
											<label class="screen-reader-text" for="s">Search for:</label> 					
											<input type="text" value="" name="s" id="s"> 					
											<input type="submit" id="searchsubmit" value="Search"> 				
										</div> 			
									</form>
								</a>
							</li>
						</ul>						
					</nav>
					
					
					
					
				<div id="et_mobile_nav_menu">
					<div class="mobile_nav closed">
						<span class="select_page">Select Page</span>
						<span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
					</div>
				</div>				
			</div> <!-- #et-top-navigation -->
			</div> <!-- .container -->
			<div class="et_search_outer">
				<div class="container et_search_form_container">
					<form role="search" method="get" class="et-search-form" action="#">
						<input type="search" class="et-search-field" placeholder="Search &hellip;" value="" name="s" title="Search for:" />					
					</form>
					<span class="et_close_search_field"></span>
				</div>
			</div>
		</header> <!-- #main-header -->
			<div id="et-main-area">
	
<div id="main-content">
	<article id="post-268" class="post-268 page type-page status-publish hentry">
	<div class="entry-content">
	<div id="et-boc" class="et-boc">
	<div class="et_builder_inner_content et_pb_gutters3"><div class="et_pb_section et_pb_section_0 et_section_regular">
	<div class="et_pb_row et_pb_row_0">			
		<div class="et_pb_column et_pb_column_4_4 et_pb_column_0    et_pb_css_mix_blend_mode_passthrough et-last-child">
			<div class="et_pb_with_border et_pb_module et_pb_cta_0 et_animated et_pb_promo et_pb_bg_layout_light  et_pb_text_align_left" style="background-color: #ffffff;">
				<div class="et_pb_promo_description">
					<h3 class="et_pb_module_header"><?= strtoupper($nama_kategori) ?></h3>
					<p align="justify"><?= $info_tambahan ?> </p>
				</div>
			</div>
		<?php 
		foreach($layanan as $layanan): 
		?>	
			<h3 class="et_pb_module_header"><?= $layanan->nama_kategori ?></h3>		
			<div class="et_pb_module et_pb_toggle et_pb_toggle_0 et_pb_toggle_item  et_pb_toggle_close">	
				<h5 class="et_pb_toggle_title">Deskripsi Layanan</h5>
				<div class="et_pb_toggle_content clearfix">
					<p align="justify">
						<?= $layanan->deskripsi ?>
					</p>
				</div> <!-- .et_pb_toggle_content -->
			</div> <!-- .et_pb_toggle -->
			
			<div class="et_pb_module et_pb_toggle et_pb_toggle_1 et_pb_toggle_item  et_pb_toggle_close">					
				<h5 class="et_pb_toggle_title">Syarat</h5>
				<div class="et_pb_toggle_content clearfix">
					<p align="justify"><?= $layanan->syarat ?></p>
				</div> <!-- .et_pb_toggle_content -->
			</div> <!-- .et_pb_toggle -->
			
			<div class="et_pb_module et_pb_toggle et_pb_toggle_2 et_pb_toggle_item  et_pb_toggle_close">
				<h5 class="et_pb_toggle_title">Prosedur Layanan</h5>
				<div class="et_pb_toggle_content clearfix">
					<p align="justify"><?= $layanan->prosedur ?></p>
				</div> <!-- .et_pb_toggle_content -->
			</div> <!-- .et_pb_toggle -->
			
			<div class="et_pb_module et_pb_toggle et_pb_toggle_3 et_pb_toggle_item  et_pb_toggle_close">
				<h5 class="et_pb_toggle_title">Biaya Layanan</h5>
				<div class="et_pb_toggle_content clearfix">
					<p align="justify"><?= $layanan->biaya ?></p>
				</div> <!-- .et_pb_toggle_content -->
			</div> <!-- .et_pb_toggle -->
			
			<div class="et_pb_module et_pb_toggle et_pb_toggle_4 et_pb_toggle_item  et_pb_toggle_close">
				<h5 class="et_pb_toggle_title">Contact</h5>
				<div class="et_pb_toggle_content clearfix">
					<p>Email: <a href="mailto:<?= $email ?>"><?= $email ?></a></p>
					<p>Telpon: <?= $kontak ?></p>
					<a href="https://api.whatsapp.com/send?phone=<?= $kontak ?>&text=Apa%20yang%20dapat%20kami%20bantu?%20Silahkan%20chat%20disini"><p><img src="<?= base_url() ?>assets/konten/unggah/2018/09/whatsapp.png" width="300px"></p></a>
					<p>&nbsp;</p>
				</div> <!-- .et_pb_toggle_content -->
			</div> <!-- .et_pb_toggle -->
			
			<div class="et_pb_module et_pb_toggle et_pb_toggle_5 et_pb_toggle_item  et_pb_toggle_close">
				<h5 class="et_pb_toggle_title">Lama Penyelesaian</h5>
				<div class="et_pb_toggle_content clearfix">
					<p align="justify"><?= $layanan->lama_penyelesaian ?></p>
				</div> <!-- .et_pb_toggle_content -->
			</div> <!-- .et_pb_toggle -->
			
			<div class="et_pb_module et_pb_toggle et_pb_toggle_6 et_pb_toggle_item  et_pb_toggle_close">
				<h5 class="et_pb_toggle_title">Waktu Layanan</h5>
				<div class="et_pb_toggle_content clearfix">
					<p align="justify"><?= $layanan->jam_layanan ?></p>
				</div> <!-- .et_pb_toggle_content -->
			</div> <!-- .et_pb_toggle -->
			
			<div class="et_pb_module et_pb_toggle et_pb_toggle_7 et_pb_toggle_item  et_pb_toggle_close">
				<h5 class="et_pb_toggle_title">Informasi Tambahan</h5>
				<div class="et_pb_toggle_content clearfix">
					<p align="justify"><?= $layanan->info_tambahan ?></p>
				</div> <!-- .et_pb_toggle_content -->
			</div> <!-- .et_pb_toggle -->
			
			<div class="et_pb_module et_pb_toggle et_pb_toggle_8 et_pb_toggle_item  et_pb_toggle_close">
				<h5 class="et_pb_toggle_title">Alamat</h5>
				<div class="et_pb_toggle_content clearfix">
					<p><?= $alamat ?></p>
				</div> <!-- .et_pb_toggle_content -->
			</div> <!-- .et_pb_toggle -->
			
			<div class="et_pb_module et_pb_toggle et_pb_toggle_9 et_pb_toggle_item  et_pb_toggle_close">
				<h5 class="et_pb_toggle_title">Koordinat</h5>
				<div class="et_pb_toggle_content clearfix">
					<p><?= $lokasi ?></p>
					<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 400px;">
							<div class="height-sm"  >
								<ul class="media-list media-list-with-divider" style="height:  120px;">
								
								<?php echo $map['js'];?>
								<?php echo $map['html'];?>
 
								</ul>
							</div>
							</div>
				</div> <!-- .et_pb_toggle_content -->
			</div> <!-- .et_pb_toggle -->
		<?php endforeach ?>
		</div> <!-- .et_pb_column -->
	</div> <!-- .et_pb_row -->
	</div> <!-- .et_pb_section -->			
	</div>
	</div>					
	</div> <!-- .entry-content -->
	</article> <!-- .et_pb_post -->

			

</div> <!-- #main-content -->


			<footer id="main-footer">
				

		
				<div id="footer-bottom">
					<div class="container clearfix">
				<ul class="et-social-icons">


</ul>					</div>	<!-- .container -->
				</div>
			</footer> <!-- #main-footer -->
		</div> <!-- #et-main-area -->


	</div> <!-- #page-container -->

	
</div>	<script type="text/javascript">
		var et_animation_data = [{"class":"et_pb_cta_0","style":"zoom","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"20%","starting_opacity":"0%","speed_curve":"ease-in-out"}];
	</script>
	<script type='text/javascript'>
/* <![CDATA[ */
var ht_ccw_var = {"page_title":"detil-layanan","google_analytics":"","ga_category":"","ga_action":"","ga_label":"","fb_analytics":"","fb_event_name":"","p1_value":"","p2_value":"","p3_value":"","p1_name":"","p2_name":"","p3_name":""};
/* ]]> */
</script>
<script type='text/javascript' src='../konten/plugins/click-to-chat-for-whatsapp/assets/js/appf24c.js?ver=1.6'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var DIVI = {"item_count":"%d Item","items_count":"%d Items"};
var et_shortcodes_strings = {"previous":"Previous","next":"Next"};
var et_pb_custom = {"ajaxurl":"http:\/\/tommy-maulana.com\/wp-admin\/admin-ajax.php","images_uri":"http:\/\/tommy-maulana.com\/konten\/amet\/Maul\/images","builder_images_uri":"http:\/\/tommy-maulana.com\/konten\/amet\/Maul\/includes\/builder\/images","et_frontend_nonce":"ffea6c734e","subscription_failed":"Please, check the fields below to make sure you entered the correct information.","et_ab_log_nonce":"886cbc4a8c","fill_message":"Please, fill in the following fields:","contact_error_message":"Please, fix the following errors:","invalid":"Invalid email","captcha":"Captcha","prev":"Prev","previous":"Previous","next":"Next","wrong_captcha":"You entered the wrong number in captcha.","is_builder_plugin_used":"","ignore_waypoints":"no","is_divi_theme_used":"1","widget_search_selector":".widget_search","is_ab_testing_active":"","page_id":"268","unique_test_id":"","ab_bounce_rate":"5","is_cache_plugin_active":"no","is_shortcode_tracking":""};
var et_pb_box_shadow_elements = [];
/* ]]> */
</script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/konten/amet/Maul/js/custom.min90d0.js?ver=3.15'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/konten/amet/Maul/core/admin/js/common90d0.js?ver=3.15'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/konten/unggah/wtfdivi/wp_footer5010.js?ver=4.9.8'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/include/js/wp-embed.min5010.js?ver=4.9.8'></script>
<style id="et-core-unified-cached-inline-styles-2">.et_pb_toggle_6.et_pb_toggle h5,.et_pb_toggle_6.et_pb_toggle h1.et_pb_toggle_title,.et_pb_toggle_6.et_pb_toggle h2.et_pb_toggle_title,.et_pb_toggle_6.et_pb_toggle h3.et_pb_toggle_title,.et_pb_toggle_6.et_pb_toggle h4.et_pb_toggle_title,.et_pb_toggle_6.et_pb_toggle h6.et_pb_toggle_title{font-family:'Rubik',Helvetica,Arial,Lucida,sans-serif;font-weight:500;text-transform:uppercase;font-size:12px;color:#000000!important;letter-spacing:2px;line-height:2em}.et_pb_toggle_5.et_pb_toggle.et_pb_toggle_close{background-color:rgba(0,0,0,0)}.et_pb_toggle_5.et_pb_toggle.et_pb_toggle_open{background-color:rgba(0,0,0,0)}.et_pb_toggle_6.et_pb_toggle p{line-height:2em}.et_pb_toggle_6.et_pb_toggle{line-height:2em;padding-top:16px!important;padding-right:12px!important;padding-bottom:16px!important;padding-left:12px!important;margin-bottom:0px!important}.et_pb_toggle_6.et_pb_toggle.et_pb_toggle_close{background-color:rgba(0,0,0,0)}.et_pb_toggle_6.et_pb_toggle.et_pb_toggle_open{background-color:rgba(0,0,0,0)}.et_pb_module.et_pb_toggle_6.et_pb_toggle{border-width:0px 0px 2px 0px;border-color:#eff5f7}.et_pb_module.et_pb_toggle_5.et_pb_toggle{border-width:0px 0px 2px 0px;border-color:#eff5f7}.et_pb_toggle_5.et_pb_toggle{line-height:2em;padding-top:16px!important;padding-right:12px!important;padding-bottom:16px!important;padding-left:12px!important;margin-bottom:0px!important}.et_pb_module.et_pb_toggle_4.et_pb_toggle{border-width:0px 0px 2px 0px;border-color:#eff5f7}.et_pb_section_0{padding-top:54px;padding-right:0px;padding-bottom:120px;padding-left:0px}.et_pb_toggle_4.et_pb_toggle p{line-height:2em}.et_pb_toggle_4.et_pb_toggle.et_pb_toggle_open{background-color:rgba(0,0,0,0)}.et_pb_toggle_4.et_pb_toggle.et_pb_toggle_close{background-color:rgba(0,0,0,0)}.et_pb_toggle_5.et_pb_toggle p{line-height:2em}.et_pb_toggle_5.et_pb_toggle h5,.et_pb_toggle_5.et_pb_toggle h1.et_pb_toggle_title,.et_pb_toggle_5.et_pb_toggle h2.et_pb_toggle_title,.et_pb_toggle_5.et_pb_toggle h3.et_pb_toggle_title,.et_pb_toggle_5.et_pb_toggle h4.et_pb_toggle_title,.et_pb_toggle_5.et_pb_toggle h6.et_pb_toggle_title{font-family:'Rubik',Helvetica,Arial,Lucida,sans-serif;font-weight:500;text-transform:uppercase;font-size:12px;color:#000000!important;letter-spacing:2px;line-height:2em}.et_pb_toggle_7.et_pb_toggle h5,.et_pb_toggle_7.et_pb_toggle h1.et_pb_toggle_title,.et_pb_toggle_7.et_pb_toggle h2.et_pb_toggle_title,.et_pb_toggle_7.et_pb_toggle h3.et_pb_toggle_title,.et_pb_toggle_7.et_pb_toggle h4.et_pb_toggle_title,.et_pb_toggle_7.et_pb_toggle h6.et_pb_toggle_title{font-family:'Rubik',Helvetica,Arial,Lucida,sans-serif;font-weight:500;text-transform:uppercase;font-size:12px;color:#000000!important;letter-spacing:2px;line-height:2em}.et_pb_toggle_7.et_pb_toggle p{line-height:2em}.et_pb_toggle_9.et_pb_toggle p{line-height:2em}.et_pb_toggle_9.et_pb_toggle h5,.et_pb_toggle_9.et_pb_toggle h1.et_pb_toggle_title,.et_pb_toggle_9.et_pb_toggle h2.et_pb_toggle_title,.et_pb_toggle_9.et_pb_toggle h3.et_pb_toggle_title,.et_pb_toggle_9.et_pb_toggle h4.et_pb_toggle_title,.et_pb_toggle_9.et_pb_toggle h6.et_pb_toggle_title{font-family:'Rubik',Helvetica,Arial,Lucida,sans-serif;font-weight:500;text-transform:uppercase;font-size:12px;color:#000000!important;letter-spacing:2px;line-height:2em}.et_pb_toggle_8.et_pb_toggle.et_pb_toggle_close{background-color:rgba(0,0,0,0)}.et_pb_toggle_9.et_pb_toggle{line-height:2em;padding-top:16px!important;padding-right:12px!important;padding-bottom:16px!important;padding-left:12px!important;margin-bottom:0px!important}.et_pb_module.et_pb_toggle_9.et_pb_toggle{border-width:0px 0px 2px 0px;border-color:#eff5f7}.et_pb_toggle_9.et_pb_toggle.et_pb_toggle_close{background-color:rgba(0,0,0,0)}.et_pb_toggle_9.et_pb_toggle.et_pb_toggle_open{background-color:rgba(0,0,0,0)}.et_pb_toggle_8.et_pb_toggle.et_pb_toggle_open{background-color:rgba(0,0,0,0)}.et_pb_module.et_pb_toggle_8.et_pb_toggle{border-width:0px 0px 2px 0px;border-color:#eff5f7}.et_pb_toggle_7.et_pb_toggle.et_pb_toggle_open{background-color:rgba(0,0,0,0)}.et_pb_module.et_pb_toggle_7.et_pb_toggle{border-width:0px 0px 2px 0px;border-color:#eff5f7}.et_pb_toggle_7.et_pb_toggle{line-height:2em;padding-top:16px!important;padding-right:12px!important;padding-bottom:16px!important;padding-left:12px!important;margin-bottom:0px!important}.et_pb_toggle_7.et_pb_toggle.et_pb_toggle_close{background-color:rgba(0,0,0,0)}.et_pb_toggle_8.et_pb_toggle h5,.et_pb_toggle_8.et_pb_toggle h1.et_pb_toggle_title,.et_pb_toggle_8.et_pb_toggle h2.et_pb_toggle_title,.et_pb_toggle_8.et_pb_toggle h3.et_pb_toggle_title,.et_pb_toggle_8.et_pb_toggle h4.et_pb_toggle_title,.et_pb_toggle_8.et_pb_toggle h6.et_pb_toggle_title{font-family:'Rubik',Helvetica,Arial,Lucida,sans-serif;font-weight:500;text-transform:uppercase;font-size:12px;color:#000000!important;letter-spacing:2px;line-height:2em}.et_pb_toggle_8.et_pb_toggle{line-height:2em;padding-top:16px!important;padding-right:12px!important;padding-bottom:16px!important;padding-left:12px!important;margin-bottom:0px!important}.et_pb_toggle_8.et_pb_toggle p{line-height:2em}.et_pb_toggle_4.et_pb_toggle h5,.et_pb_toggle_4.et_pb_toggle h1.et_pb_toggle_title,.et_pb_toggle_4.et_pb_toggle h2.et_pb_toggle_title,.et_pb_toggle_4.et_pb_toggle h3.et_pb_toggle_title,.et_pb_toggle_4.et_pb_toggle h4.et_pb_toggle_title,.et_pb_toggle_4.et_pb_toggle h6.et_pb_toggle_title{font-family:'Rubik',Helvetica,Arial,Lucida,sans-serif;font-weight:500;text-transform:uppercase;font-size:12px;color:#000000!important;letter-spacing:2px;line-height:2em}.et_pb_toggle_4.et_pb_toggle{line-height:2em;padding-top:16px!important;padding-right:12px!important;padding-bottom:16px!important;padding-left:12px!important;margin-bottom:0px!important}.et_pb_toggle_0.et_pb_toggle.et_pb_toggle_open{background-color:rgba(0,0,0,0)}.et_pb_module.et_pb_toggle_0.et_pb_toggle{border-width:0px 0px 2px 0px;border-color:#eff5f7}.et_pb_toggle_0.et_pb_toggle{line-height:2em;padding-top:16px!important;padding-right:12px!important;padding-bottom:16px!important;padding-left:12px!important;margin-bottom:0px!important}.et_pb_toggle_0.et_pb_toggle.et_pb_toggle_close{background-color:rgba(0,0,0,0)}.et_pb_toggle_1.et_pb_toggle h5,.et_pb_toggle_1.et_pb_toggle h1.et_pb_toggle_title,.et_pb_toggle_1.et_pb_toggle h2.et_pb_toggle_title,.et_pb_toggle_1.et_pb_toggle h3.et_pb_toggle_title,.et_pb_toggle_1.et_pb_toggle h4.et_pb_toggle_title,.et_pb_toggle_1.et_pb_toggle h6.et_pb_toggle_title{font-family:'Rubik',Helvetica,Arial,Lucida,sans-serif;font-weight:500;text-transform:uppercase;font-size:12px;color:#000000!important;letter-spacing:2px;line-height:2em}.et_pb_toggle_1.et_pb_toggle{line-height:2em;padding-top:16px!important;padding-right:12px!important;padding-bottom:16px!important;padding-left:12px!important;margin-bottom:0px!important}.et_pb_toggle_1.et_pb_toggle p{line-height:2em}.et_pb_toggle_0.et_pb_toggle p{line-height:2em}.et_pb_toggle_0.et_pb_toggle h5,.et_pb_toggle_0.et_pb_toggle h1.et_pb_toggle_title,.et_pb_toggle_0.et_pb_toggle h2.et_pb_toggle_title,.et_pb_toggle_0.et_pb_toggle h3.et_pb_toggle_title,.et_pb_toggle_0.et_pb_toggle h4.et_pb_toggle_title,.et_pb_toggle_0.et_pb_toggle h6.et_pb_toggle_title{font-family:'Rubik',Helvetica,Arial,Lucida,sans-serif;font-weight:500;text-transform:uppercase;font-size:12px;color:#000000!important;letter-spacing:2px;line-height:2em}.et_pb_cta_0.et_pb_promo{line-height:2em;background-size:initial;background-position:bottom right;background-image:url(../konten/uploads/2018/09/notary-blurb-illustration-4.png);border-width:2px;border-color:#eff5f7;padding-right:100px!important}.et_pb_cta_0.et_pb_promo p{line-height:2em}.et_pb_cta_0.et_pb_promo h2,.et_pb_cta_0.et_pb_promo h1.et_pb_module_header,.et_pb_cta_0.et_pb_promo h3.et_pb_module_header,.et_pb_cta_0.et_pb_promo h4.et_pb_module_header,.et_pb_cta_0.et_pb_promo h5.et_pb_module_header,.et_pb_cta_0.et_pb_promo h6.et_pb_module_header{font-family:'Playfair Display',Georgia,"Times New Roman",serif!important;font-weight:700!important;line-height:1.5em!important}body #page-container .et_pb_cta_0.et_pb_promo .et_pb_button{color:#ef3b24!important;background:rgba(255,255,255,0);border-color:rgba(0,0,0,0);border-radius:0px;letter-spacing:2px;font-size:12px;font-family:'Rubik',Helvetica,Arial,Lucida,sans-serif!important;font-weight:500!important;text-transform:uppercase!important;padding-left:0.7em;padding-right:2em;background-color:rgba(255,255,255,0)}body #page-container .et_pb_cta_0.et_pb_promo .et_pb_button:hover{letter-spacing:2px}body #page-container .et_pb_cta_0.et_pb_promo .et_pb_button:after{line-height:1.7em;font-size:1em!important;opacity:1;margin-left:.3em;left:auto}body #page-container .et_pb_cta_0.et_pb_promo .et_pb_button:hover:after{margin-left:.3em;left:auto;margin-left:.3em}.et_pb_toggle_1.et_pb_toggle.et_pb_toggle_open{background-color:rgba(0,0,0,0)}.et_pb_module.et_pb_toggle_1.et_pb_toggle{border-width:0px 0px 2px 0px;border-color:#eff5f7}.et_pb_toggle_3.et_pb_toggle h5,.et_pb_toggle_3.et_pb_toggle h1.et_pb_toggle_title,.et_pb_toggle_3.et_pb_toggle h2.et_pb_toggle_title,.et_pb_toggle_3.et_pb_toggle h3.et_pb_toggle_title,.et_pb_toggle_3.et_pb_toggle h4.et_pb_toggle_title,.et_pb_toggle_3.et_pb_toggle h6.et_pb_toggle_title{font-family:'Rubik',Helvetica,Arial,Lucida,sans-serif;font-weight:500;text-transform:uppercase;font-size:12px;color:#000000!important;letter-spacing:2px;line-height:2em}.et_pb_toggle_2.et_pb_toggle.et_pb_toggle_close{background-color:rgba(0,0,0,0)}.et_pb_toggle_1.et_pb_toggle.et_pb_toggle_close{background-color:rgba(0,0,0,0)}.et_pb_toggle_3.et_pb_toggle p{line-height:2em}.et_pb_toggle_3.et_pb_toggle{line-height:2em;padding-top:16px!important;padding-right:12px!important;padding-bottom:16px!important;padding-left:12px!important;margin-bottom:0px!important}.et_pb_toggle_3.et_pb_toggle.et_pb_toggle_close{background-color:rgba(0,0,0,0)}.et_pb_toggle_3.et_pb_toggle.et_pb_toggle_open{background-color:rgba(0,0,0,0)}.et_pb_module.et_pb_toggle_3.et_pb_toggle{border-width:0px 0px 2px 0px;border-color:#eff5f7}.et_pb_module.et_pb_toggle_2.et_pb_toggle{border-width:0px 0px 2px 0px;border-color:#eff5f7}.et_pb_toggle_2.et_pb_toggle.et_pb_toggle_open{background-color:rgba(0,0,0,0)}.et_pb_toggle_2.et_pb_toggle h5,.et_pb_toggle_2.et_pb_toggle h1.et_pb_toggle_title,.et_pb_toggle_2.et_pb_toggle h2.et_pb_toggle_title,.et_pb_toggle_2.et_pb_toggle h3.et_pb_toggle_title,.et_pb_toggle_2.et_pb_toggle h4.et_pb_toggle_title,.et_pb_toggle_2.et_pb_toggle h6.et_pb_toggle_title{font-family:'Rubik',Helvetica,Arial,Lucida,sans-serif;font-weight:500;text-transform:uppercase;font-size:12px;color:#000000!important;letter-spacing:2px;line-height:2em}.et_pb_toggle_2.et_pb_toggle{line-height:2em;padding-top:16px!important;padding-right:12px!important;padding-bottom:16px!important;padding-left:12px!important;margin-bottom:0px!important}.et_pb_toggle_2.et_pb_toggle p{line-height:2em}.et_pb_toggle_1 .et_pb_toggle_title:before{color:#ef3b24}.et_pb_toggle_3 .et_pb_toggle_title:before{color:#ef3b24}.et_pb_toggle_6 .et_pb_toggle_title:before{color:#ef3b24}.et_pb_toggle_9 .et_pb_toggle_title:before{color:#ef3b24}.et_pb_toggle_7 .et_pb_toggle_title:before{color:#ef3b24}.et_pb_toggle_4 .et_pb_toggle_title:before{color:#ef3b24}.et_pb_toggle_0 .et_pb_toggle_title:before{color:#ef3b24}.et_pb_toggle_8 .et_pb_toggle_title:before{color:#ef3b24}.et_pb_toggle_5 .et_pb_toggle_title:before{color:#ef3b24}.et_pb_toggle_2 .et_pb_toggle_title:before{color:#ef3b24}@media only screen and (max-width:767px){.et_pb_cta_0.et_pb_promo{padding-right:40px!important;padding-bottom:160px!important}}</style></body>

</html>
