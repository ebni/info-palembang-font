<!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<script type="text/javascript">
			document.documentElement.className = 'js';
		</script>
		
		<title>Pusat Informasi Kota Palembang</title>
		
		<link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
		<link rel='dns-prefetch' href='http://s.w.org/' />
		<meta content="Divi v.3.15" name="generator"/>
		<link rel='stylesheet' id='wtfdivi-user-css-css'  href='<?php echo base_url(); ?>assets/konten/unggah/wtfdivi/wp_head5010.css?ver=4.9.8' type='text/css' media='all' />
		<link rel='stylesheet' id='divi-fonts-css'  href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&amp;subset=latin,latin-ext' type='text/css' media='all' />
		<link rel='stylesheet' id='divi-style-css'  href='<?php echo base_url(); ?>assets/konten/tema/Maulana/style90d0.css?ver=3.15' type='text/css' media='all' />
		<link rel='stylesheet' id='dashicons-css'  href='<?php echo base_url(); ?>assets/rangkuman/css/dashicons.min5010.css?ver=4.9.8' type='text/css' media='all' />
		<script type='text/javascript' src='<?php echo base_url(); ?>assets/rangkuman/js/jquery/jqueryb8ff.js?ver=1.12.4'></script>
		<script type='text/javascript' src='<?php echo base_url(); ?>assets/rangkuman/js/jquery/jquery-migrate.min330a.js?ver=1.4.1'></script>
		<link rel="canonical" href="<?php echo base_url(); ?>" />
		<style>
			.db_pb_team_member_website_icon:before{content:"\e0e3";}
			.db_pb_team_member_email_icon:before{content:"\e010";}
			.db_pb_team_member_instagram_icon:before{content:"\e09a";}
		</style>
		<style>#et_builder_outer_content .db_pb_button_2,.db_pb_button_2{margin-left:30px}</style>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
		<link rel="stylesheet" id="et-core-unified-cached-inline-styles" href="<?php echo base_url(); ?>assets/konten/cache/et/2/et-core-unified-15381462951132.min.css" onerror="et_core_page_resource_fallback(this, true)" onload="et_core_page_resource_fallback(this)" />
	</head>
	<body class="page-template-default page page-id-2 et_pb_button_helper_class et_fixed_nav et_show_nav et_cover_background et_pb_gutter windows et_pb_gutters3 et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_pb_footer_columns3 et_header_style_left et_pb_pagebuilder_layout et_right_sidebar et_divi_theme et-db et_minified_js et_minified_css">
		<div id="page-container">
			<header id="main-header" data-height-onload="66">
				<div class="container clearfix et_menu_container">
					<div class="logo_container">
						<span class="logo_helper"></span>
						<a href="<?= base_url() ?>">
							<img src="<?php echo base_url(); ?>assets/konten/unggah/2018/09/logo-1.png" alt="tommy-maulana" id="logo" style="max-height: 50px !important;" />
						</a>
					</div>
					<div id="et-top-navigation" data-height="66" data-fixed-height="66">
						<nav id="top-menu-nav">
							<ul id="top-menu" class="nav">
								<li id="menu-item-273" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-273"><a href="<?php echo base_url(); ?>">Home</a></li>
								<li id="menu-item-273" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-273"><a href="<?= base_url() ?>kategori">Layanan</a></li>
								<li id="menu-item-271" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-271"><a href="#">Profil Kami</a></li>
								<li id="menu-item-272" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-272"><a href="#">Hubungi Kami</a></li>
							</ul>						
						</nav>
						<div id="et_mobile_nav_menu">
							<div class="mobile_nav closed">
								<span class="select_page">Select Page</span>
								<span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
							</div>
						</div>				
					</div> <!-- #et-top-navigation -->
				</div> <!-- .container -->
				<div class="et_search_outer">
					<div class="container et_search_form_container">
						<form role="search" method="get" class="et-search-form" action="#">
							<input type="search" class="et-search-field" placeholder="Search &hellip;" value="" name="s" title="Search for:" />					
						</form>
						<span class="et_close_search_field"></span>
					</div>
				</div>
			</header> <!-- #main-header -->
			
			<div id="et-main-area">
				<div id="main-content">
					<article id="post-2" class="post-2 page type-page status-publish hentry">
						<div class="entry-content">
							<div id="et-boc" class="et-boc">
								<div class="et_builder_inner_content et_pb_gutters3">
									<div class="et_pb_section et_pb_section_0 et_animated et_section_regular">
										<div class="et_pb_row et_pb_row_0">
											<div class="et_pb_column et_pb_column_1_2 et_pb_column_0    et_pb_css_mix_blend_mode_passthrough">
												<div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_dark  et_pb_text_align_left">
													<div class="et_pb_text_inner">
														<h1>Pusat Informasi Kota Palembang</h1>
													</div>
												</div> <!-- .et_pb_text -->
												
												<div class="et_pb_module et_pb_text et_pb_text_1 et_pb_bg_layout_light  et_pb_text_align_left">
													<div class="et_pb_text_inner">
														<p style="text-align: justify;">Pusat Informasi Kota Palembang, merupakan layanan informasi yang dapat dimanfaatkan dan diakses secara langsung oleh masyarakat. Mulai dari perizinan, hingga informasi mengenai ketersediaan kamar di RS</p>
													</div
													>
												</div> <!-- .et_pb_text -->
												<div class="et_pb_button_module_wrapper et_pb_button_0_wrapper et_pb_button_alignment_ et_pb_module ">
													<a class="et_pb_button et_pb_custom_button_icon et_pb_button_0 et_animated et_pb_bg_layout_light" href="<?= base_url() ?>kategori/0/0" data-icon="&#x45;">Lihat Layanan</a>
												</div>
											</div> <!-- .et_pb_column -->
											<div class="et_pb_column et_pb_column_1_2 et_pb_column_1    et_pb_css_mix_blend_mode_passthrough et-last-child">
												<div class="et_pb_module et_pb_image et_pb_image_0 et_animated et-waypoint et_always_center_on_mobile">
													<span class="et_pb_image_wrap"><img src="<?php echo base_url(); ?>assets/konten/unggah/2018/09/customer-service-1.png" alt="" /></span>
												</div>
											</div> <!-- .et_pb_column -->
										</div> <!-- .et_pb_row -->
									</div> <!-- .et_pb_section -->
									
									<div class="et_pb_section et_pb_section_1 et_section_specialty">
										<div class="et_pb_row">
											<div class="et_pb_column et_pb_column_1_3 et_pb_column_2    et_pb_css_mix_blend_mode_passthrough">
												<div class="et_pb_module et_pb_text et_pb_text_2 et_animated et_pb_bg_layout_light  et_pb_text_align_left">
													<div class="et_pb_text_inner">
														<h1>Pelayanan Online</h1>
													</div>
												</div> <!-- .et_pb_text -->
												<div class="et_pb_module et_pb_divider et_pb_divider_0 et_animated et_pb_divider_position_ et_pb_space">
													<div class="et_pb_divider_internal"></div>
												</div>
												<div class="et_pb_module et_pb_text et_pb_text_3 et_animated et_pb_bg_layout_light  et_pb_text_align_left">
													<div class="et_pb_text_inner">
														<p><span style="color: #2e2545;"><strong>Pelayanan online meliputi perizinan dan sejenisnya</strong></span></p>
														<p style="text-align: justify;">Dinas pendidikan, BPPD, RSUD Bari, Kecamatan, Dinas Kesehatan, Dinas Perhubungan, Dinas Kebakaran, PTSP, dan Dinas Kependudukan &amp; Catatan Sipil</p>
													</div>
												</div> <!-- .et_pb_text -->
												<div class="et_pb_button_module_wrapper et_pb_button_1_wrapper et_pb_button_alignment_left et_pb_module ">
													<a class="et_pb_button et_pb_custom_button_icon et_pb_button_1 et_animated et_pb_bg_layout_light" href="<?= base_url() ?>kategori/0/0" data-icon="&#x45;">LAYANAN KAMI</a>
												</div>
												<div class="et_pb_button_module_wrapper et_pb_button_2_wrapper et_pb_button_alignment_left et_pb_module ">
													<a class="et_pb_button et_pb_custom_button_icon et_pb_button_2 et_animated et_pb_bg_layout_light" href="<?= base_url() ?>jadwal" data-icon="&#x45;">JADWAL WALIKOTA</a>
												</div>
												<div class="et_pb_button_module_wrapper et_pb_button_3_wrapper et_pb_button_alignment_left et_pb_module ">
													<a class="et_pb_button et_pb_custom_button_icon et_pb_button_3 et_animated et_pb_bg_layout_light" href="#" data-icon="&#x45;">LAPORAN WARGA</a>
												</div>
											</div> <!-- .et_pb_column -->
											<div class="et_pb_column et_pb_column_2_3 et_pb_column_3   et_pb_specialty_column  et_pb_css_mix_blend_mode_passthrough et-last-child">
												<div class="et_pb_row_inner et_pb_row_inner_0 et_animated et_pb_gutters1">
												<?php 
												foreach($kategori as $kategori): 
												?>	
													<div class="et_pb_column et_pb_column_1_3 et_pb_column_inner et_pb_column_inner_0  et_pb_column_1_2">
														<div class="et_pb_module et_pb_blurb et_pb_blurb_0 et_animated et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_left">
															<div class="et_pb_blurb_content">
																<div class="et_pb_main_blurb_image">
																	<a href="<?= base_url() ?>kategori/<?= $kategori->kd_kategori ?>/<?= $kategori->kd_kategori ?>">
																		<span class="et_pb_image_wrap">
																			<img src="<?= base_url(); ?>assets/img/<?= $kategori->logo ?>" alt="" class="et-waypoint et_pb_animation_top" />
																		</span>
																	</a>
																</div>
																<div class="et_pb_blurb_container">
																	<h4 class="et_pb_module_header">
																		<a href="<?= base_url() ?>kategori/<?= $kategori->kd_kategori ?>/<?= $kategori->kd_kategori ?>"> 
																			<?= $kategori->nama_kategori ?> 
																		</a>
																	</h4>
																	<div class="et_pb_blurb_description">
																		<p style="text-align: justify; color: #494949;"><?= $kategori->info_tambahan ?></p>
																	</div><!-- .et_pb_blurb_description -->
																</div>
															</div> <!-- .et_pb_blurb_content -->
														</div> <!-- .et_pb_blurb -->
													</div> <!-- .et_pb_column -->
												<?php endforeach ?>
												</div> <!-- .et_pb_row_inner -->
											</div> <!-- .et_pb_column -->
										</div> <!-- .et_pb_row -->		
									</div> <!-- .et_pb_section -->
									
									<div class="et_pb_section et_pb_section_2 et_pb_with_background et_section_regular">
										<div class="et_pb_row et_pb_row_1 et_animated">
											<div class="et_pb_column et_pb_column_1_2 et_pb_column_4    et_pb_css_mix_blend_mode_passthrough">
												<div class="et_pb_module et_pb_image et_pb_image_1 et_animated et-waypoint et_always_center_on_mobile">	
													<span class="et_pb_image_wrap"><img src="<?php echo base_url(); ?>assets/konten/unggah/2018/09/coding-isometric-02.png" alt="" /></span>
												</div>
											</div> <!-- .et_pb_column -->
											<div class="et_pb_column et_pb_column_1_2 et_pb_column_5    et_pb_css_mix_blend_mode_passthrough et-last-child">
												<div class="et_pb_module et_pb_text et_pb_text_4 et_animated et_pb_bg_layout_dark  et_pb_text_align_left">
													<div class="et_pb_text_inner">
														<h1>Profil Pusat Informasi</h1>
													</div>
												</div> <!-- .et_pb_text -->
												<div class="et_pb_module et_pb_divider et_pb_divider_1 et_animated et_pb_divider_position_ et_pb_space">
													<div class="et_pb_divider_internal"></div>
												</div>
												<div class="et_pb_module et_pb_text et_pb_text_5 et_animated et_pb_bg_layout_light  et_pb_text_align_left">
													<div class="et_pb_text_inner">
														<p style="text-align: justify;">Pusat Informasi Kota Palembang ditujukan untuk menyempurnakan pelayanan publik dari Pemerintah Kota Palembang kepada masyarakat dan menjadi salah satu inovasi Pemerintah Kota Palembang dalam hal pelayanan publik serta diharapkan menjadi salah satu kunci penting dalam upaya Pemerintah Kota Palembang untuk mewujudkan Palembang Smart City</p>
													</div>
												</div> <!-- .et_pb_text -->
												<div class="et_pb_button_module_wrapper et_pb_button_4_wrapper et_pb_button_alignment_ et_pb_module ">
													<a class="et_pb_button et_pb_custom_button_icon et_pb_button_4 et_animated et_pb_bg_layout_light" href="#" data-icon="&#x45;">profil kami</a>
												</div>
											</div> <!-- .et_pb_column -->
										</div> <!-- .et_pb_row -->
									</div> <!-- .et_pb_section -->
									
									<div class="et_pb_section et_pb_section_3 et_section_regular">
										<div class="et_pb_row et_pb_row_2">
											<div class="et_pb_column et_pb_column_1_3 et_pb_column_6    et_pb_css_mix_blend_mode_passthrough">
												<div class="et_pb_module et_pb_text et_pb_text_6 et_animated et_pb_bg_layout_light  et_pb_text_align_left">
													<div class="et_pb_text_inner">
														<h1>FAQ</h1>
													</div>
												</div> <!-- .et_pb_text -->
												<div class="et_pb_module et_pb_divider et_pb_divider_2 et_animated et_pb_divider_position_ et_pb_space">
													<div class="et_pb_divider_internal"></div>
												</div>
												<div class="et_pb_module et_pb_text et_pb_text_7 et_animated et_pb_bg_layout_light  et_pb_text_align_left">
													<div class="et_pb_text_inner">
														<p><span style="color: #2e2545;"><strong>Pertanyaan-pertanyaan yang sering diajukan</strong></span></p>
														<p>Temukan jawaban dari hal-hal yang paling sering ditanyakan mengenai Pusat Informasi Kota Palembang. Jika tidak menemukan jawaban yang Anda cari, silakan klik tombol dibawah:</p>
													</div>
												</div> <!-- .et_pb_text -->
												<div class="et_pb_button_module_wrapper et_pb_button_5_wrapper et_pb_button_alignment_left et_pb_module ">
													<a class="et_pb_button et_pb_custom_button_icon et_pb_button_5 et_animated et_pb_bg_layout_light" href="#" target="_blank" data-icon="&#x45;">Hubungi Kami</a>
												</div>
											</div> <!-- .et_pb_column -->
											<div class="et_pb_column et_pb_column_1_3 et_pb_column_7    et_pb_css_mix_blend_mode_passthrough">
												<div class="et_pb_module et_pb_blurb et_pb_blurb_8 et_animated et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_left">
													<div class="et_pb_blurb_content">
														<div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-pb-icon et-waypoint et_pb_animation_top" style="color: #09e1c0;">&#xe064;</span></span></div>
														<div class="et_pb_blurb_container">
															<h4 class="et_pb_module_header">Apakah Pusat Informasi Kota Palembang?</h4>
															<div class="et_pb_blurb_description">
																<p style="text-align: justify;">Temukan jawaban dari hal-hal yang paling sering diajukan warga dan masyarakat mengenai Pusat Informasi Kota Palembang</p>
															</div><!-- .et_pb_blurb_description -->
														</div>
													</div> <!-- .et_pb_blurb_content -->
												</div> <!-- .et_pb_blurb -->
												<div class="et_pb_module et_pb_blurb et_pb_blurb_9 et_animated et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_left">
													<div class="et_pb_blurb_content">
														<div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-pb-icon et-waypoint et_pb_animation_top" style="color: #09e1c0;">&#xe064;</span></span></div>
														<div class="et_pb_blurb_container">
															<h4 class="et_pb_module_header">Dinas Apa Saja yang sudah terhubung disini?</h4>
															<div class="et_pb_blurb_description">
																<p style="text-align: justify;">Temukan jawaban dari hal-hal yang paling sering diajukan warga dan masyarakat mengenai Pusat Informasi Kota Palembang</p>
															</div><!-- .et_pb_blurb_description -->
														</div>
													</div> <!-- .et_pb_blurb_content -->
												</div> <!-- .et_pb_blurb -->
												<div class="et_pb_module et_pb_blurb et_pb_blurb_10 et_animated et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_left">
													<div class="et_pb_blurb_content">
														<div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-pb-icon et-waypoint et_pb_animation_top" style="color: #09e1c0;">&#xe064;</span></span></div>
														<div class="et_pb_blurb_container">
															<h4 class="et_pb_module_header">Daerah mana saja yang bisa menikmati layanan ini?</h4>
															<div class="et_pb_blurb_description">
																<p style="text-align: justify;">Temukan jawaban dari hal-hal yang paling sering diajukan warga dan masyarakat mengenai Pusat Informasi Kota Palembang</p>
															</div><!-- .et_pb_blurb_description -->
														</div>
													</div> <!-- .et_pb_blurb_content -->
												</div> <!-- .et_pb_blurb -->
											</div> <!-- .et_pb_column -->
											
											<div class="et_pb_column et_pb_column_1_3 et_pb_column_8    et_pb_css_mix_blend_mode_passthrough et-last-child">
												<div class="et_pb_module et_pb_blurb et_pb_blurb_11 et_animated et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_left">
													<div class="et_pb_blurb_content">
														<div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-pb-icon et-waypoint et_pb_animation_top" style="color: #09e1c0;">&#xe064;</span></span></div>
														<div class="et_pb_blurb_container">
															<h4 class="et_pb_module_header">Layanan Apa Saja yang Tersedia disini?</h4>
															<div class="et_pb_blurb_description">
																<p style="text-align: justify;">Temukan jawaban dari hal-hal yang paling sering diajukan warga dan masyarakat mengenai Pusat Informasi Kota Palembang</p>
															</div><!-- .et_pb_blurb_description -->
														</div>
													</div> <!-- .et_pb_blurb_content -->
												</div> <!-- .et_pb_blurb -->
												<div class="et_pb_module et_pb_blurb et_pb_blurb_12 et_animated et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_left">
													<div class="et_pb_blurb_content">
														<div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-pb-icon et-waypoint et_pb_animation_top" style="color: #09e1c0;">&#xe064;</span></span></div>
														<div class="et_pb_blurb_container">
															<h4 class="et_pb_module_header">Apakah saya bisa mendaftar izin secara daring?</h4>
															<div class="et_pb_blurb_description">
																<p style="text-align: justify;">Temukan jawaban dari hal-hal yang paling sering diajukan warga dan masyarakat mengenai Pusat Informasi Kota Palembang</p>
															</div><!-- .et_pb_blurb_description -->
														</div>
													</div> <!-- .et_pb_blurb_content -->
												</div> <!-- .et_pb_blurb -->
												<div class="et_pb_module et_pb_blurb et_pb_blurb_13 et_animated et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_left">
													<div class="et_pb_blurb_content">
														<div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-pb-icon et-waypoint et_pb_animation_top" style="color: #09e1c0;">&#xe064;</span></span></div>
														<div class="et_pb_blurb_container">
															<h4 class="et_pb_module_header">Apakah perizinan daring ini legal dan tercatat?</h4>
															<div class="et_pb_blurb_description">
																<p style="text-align: justify;">Temukan jawaban dari hal-hal yang paling sering diajukan warga dan masyarakat mengenai Pusat Informasi Kota Palembang</p>
															</div><!-- .et_pb_blurb_description -->
														</div>
													</div> <!-- .et_pb_blurb_content -->
												</div> <!-- .et_pb_blurb -->
											</div> <!-- .et_pb_column -->
										</div> <!-- .et_pb_row -->
									</div> <!-- .et_pb_section -->			
								</div>		
							</div>					
						</div> <!-- .entry-content -->
					</article> <!-- .et_pb_post -->
				</div> <!-- #main-content -->

				<footer id="main-footer">
					<div id="footer-bottom">
						<div class="container clearfix"></div>	<!-- .container -->
					</div>
				</footer> <!-- #main-footer -->
			</div> <!-- #et-main-area -->
		</div> <!-- #page-container -->

		<script type="text/javascript">
			var et_animation_data = [{"class":"et_pb_section_0","style":"slideTop","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"2%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_button_0","style":"zoom","repeat":"once","duration":"1000ms","delay":"100ms","intensity":"10%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_image_0","style":"zoomLeft","repeat":"once","duration":"1000ms","delay":"100ms","intensity":"20%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_text_2","style":"slideBottom","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"10%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_divider_0","style":"zoomLeft","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"50%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_text_3","style":"zoom","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"6%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_button_1","style":"zoom","repeat":"once","duration":"1000ms","delay":"100ms","intensity":"6%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_button_2","style":"zoom","repeat":"once","duration":"1000ms","delay":"100ms","intensity":"6%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_button_3","style":"zoom","repeat":"once","duration":"1000ms","delay":"100ms","intensity":"6%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_row_inner_0","style":"zoom","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"6%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_blurb_0","style":"zoomBottom","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"20%","starting_opacity":"100%","speed_curve":"ease-in-out"},{"class":"et_pb_blurb_1","style":"zoomBottom","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"20%","starting_opacity":"100%","speed_curve":"ease-in-out"},{"class":"et_pb_blurb_2","style":"zoomBottom","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"20%","starting_opacity":"100%","speed_curve":"ease-in-out"},{"class":"et_pb_blurb_3","style":"zoomBottom","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"20%","starting_opacity":"100%","speed_curve":"ease-in-out"},{"class":"et_pb_blurb_4","style":"zoomBottom","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"20%","starting_opacity":"100%","speed_curve":"ease-in-out"},{"class":"et_pb_blurb_5","style":"zoomBottom","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"20%","starting_opacity":"100%","speed_curve":"ease-in-out"},{"class":"et_pb_blurb_6","style":"zoomBottom","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"20%","starting_opacity":"100%","speed_curve":"ease-in-out"},{"class":"et_pb_blurb_7","style":"zoomBottom","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"20%","starting_opacity":"100%","speed_curve":"ease-in-out"},{"class":"et_pb_row_1","style":"zoomBottom","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"6%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_image_1","style":"zoomRight","repeat":"once","duration":"1000ms","delay":"100ms","intensity":"20%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_text_4","style":"slideBottom","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"10%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_divider_1","style":"zoomLeft","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"50%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_text_5","style":"zoom","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"6%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_button_4","style":"zoom","repeat":"once","duration":"1000ms","delay":"100ms","intensity":"10%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_text_6","style":"slideBottom","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"10%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_divider_2","style":"zoomLeft","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"50%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_text_7","style":"zoom","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"6%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_button_5","style":"zoom","repeat":"once","duration":"1000ms","delay":"100ms","intensity":"6%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_blurb_8","style":"zoomLeft","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"10%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_blurb_9","style":"zoomLeft","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"10%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_blurb_10","style":"zoomLeft","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"10%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_blurb_11","style":"zoomLeft","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"10%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_blurb_12","style":"zoomLeft","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"10%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_blurb_13","style":"zoomLeft","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"10%","starting_opacity":"0%","speed_curve":"ease-in-out"}];
		</script>
		<script type='text/javascript'>
			/* <![CDATA[ */
			var DIVI = {"item_count":"%d Item","items_count":"%d Items"};
			var et_shortcodes_strings = {"previous":"Previous","next":"Next"};
			var et_pb_custom = {"ajaxurl":"http:\/\/tommy-maulana.com\/wp-admin\/admin-ajax.php","images_uri":"http:\/\/tommy-maulana.com\/konten\/tema\/Maulana\/images","builder_images_uri":"http:\/\/tommy-maulana.com\/konten\/tema\/Maulana\/includes\/builder\/images","et_frontend_nonce":"617fe5d187","subscription_failed":"Please, check the fields below to make sure you entered the correct information.","et_ab_log_nonce":"6b9fad679e","fill_message":"Please, fill in the following fields:","contact_error_message":"Please, fix the following errors:","invalid":"Invalid email","captcha":"Captcha","prev":"Prev","previous":"Previous","next":"Next","wrong_captcha":"You entered the wrong number in captcha.","is_builder_plugin_used":"","ignore_waypoints":"no","is_divi_theme_used":"1","widget_search_selector":".widget_search","is_ab_testing_active":"","page_id":"2","unique_test_id":"","ab_bounce_rate":"5","is_cache_plugin_active":"no","is_shortcode_tracking":""};
			var et_pb_box_shadow_elements = [];
			/* ]]> */
		</script>
		<script type='text/javascript' src='<?php echo base_url(); ?>assets/konten/tema/Maulana/js/custom.min90d0.js?ver=3.15'></script>
		<script type='text/javascript' src='<?php echo base_url(); ?>assets/konten/tema/Maulana/core/admin/js/common90d0.js?ver=3.15'></script>
		<script type='text/javascript' src='<?php echo base_url(); ?>assets/konten/unggah/wtfdivi/wp_footer5010.js?ver=4.9.8'></script>
		<script type='text/javascript' src='<?php echo base_url(); ?>assets/rangkuman/js/wp-embed.min5010.js?ver=4.9.8'></script>
	</body>
<!-- Mirrored from tommy-maulana.com/prototype/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 29 Sep 2018 00:09:00 GMT -->
</html>
