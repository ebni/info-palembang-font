<?php
	//Get list of option for dropdown.
	function getDropdownList($table, $columns)
	{
		$CI =& get_instance();
		$query = $CI->db->select($columns)->from($table)->get();
		
		if($query->num_rows() >= 1){
			$options1 = ['' => '- Pilih -'];
			$options2 = array_column($query->result_array(), $columns[1], $columns[0]);
			$options = $options1 + $options2;
			return $options;
		}
		return $options = ['' => '- Pilih -'];
	}
		
	function getDropdownJoin($table, $table2, $id, $id2, $columns)
	{
		$CI =& get_instance();
		$query = $CI->db->select($columns)->join($table2, "$table.$id = $table2.$id2", 'left')->from($table)->get();
		
		if($query->num_rows() >= 1){
			$options1 = ['' => '- Pilih -'];
			$options2 = array_column($query->result_array(), $columns[1], $columns[0]);
			$options = $options1 + $options2;
			return $options;
		}
		return $options = ['' => '- Pilih -'];
	}
	
	function getDropdownListwhere($table, $columns, $field, $key)
	{
		$CI =& get_instance();
		$query = $CI->db->select($columns)->where($field, $key)->from($table)->get();
		
		if($query->num_rows() >= 1){
			$options1 = ['' => '- Pilih -'];
			$options2 = array_column($query->result_array(), $columns[1], $columns[0]);
			$options = $options1 + $options2;
			return $options;
		}
		return $options = ['' => '- Pilih -'];
	}
	
	function getDropdownListwhere2($table, $columns, $field, $key, $field2)
	{
		$CI =& get_instance();
		$query = $CI->db->select($columns)->where($field, $key)->where_not_in($field2, '')->where_not_in('kategori_kode', 'kegiatan')->where_not_in('kategori_kode', 'output')->from($table)->get();
		
		if($query->num_rows() >= 1){
			$options1 = ['' => '- Pilih -'];
			$options2 = array_column($query->result_array(), $columns[1], $columns[0]);
			$options = $options1 + $options2;
			return $options;
		}
		return $options = ['' => '- Pilih -'];
	}
	
	// Show form error validation message for "fille" input.
	function fileFormError($field, $prefix = '', $suffix = '')
	{
		$CI =& get_instance();
		$error_field = $CI->form_validation->error_array();
		
		if (!empty($error_field[$field])){
			return $prefix . $error_field[$field] . $suffix;
		}
		return '';
	}
	
		function to_word($number)
	{
		$words = "";
		$arr_number = array(
		"",
		"Satu",
		"Dua",
		"Tiga",
		"Empat",
		"Lima",
		"Enam",
		"Tujuh",
		"Delapan",
		"Sembilan",
		"Sepuluh",
		"Sebelas");

		if($number<12)
		{
			$words = " ".$arr_number[$number];
		}
		else if($number<20)
		{
			$words = to_word($number-10)." Belas";
		}
		else if($number<100)
		{
			$words = to_word($number/10)." Puluh ".to_word($number%10);
		}
		else if($number<200)
		{
			$words = "seratus ".to_word($number-100);
		}
		else if($number<1000)
		{
			$words = to_word($number/100)." Ratus ".to_word($number%100);
		}
		else if($number<2000)
		{
			$words = "seribu ".to_word($number-1000);
		}
		else if($number<1000000)
		{
			$words = to_word($number/1000)." Ribu ".to_word($number%1000);
		}
		else if($number<1000000000)
		{
			$words = to_word($number/1000000)." Juta ".to_word($number%1000000);
		}
		else
		{
			$words = "undefined";
		}
		return $words;
	}

	
if ( ! function_exists('tgl_indo'))
{
	function tgl_indo($tgl)
	{
		$ubah = gmdate($tgl, time()+60*60*8);
		$pecah = explode("-",$ubah);
		$tanggal = $pecah[2];
		$bulan = bulan($pecah[1]);
		$tahun = $pecah[0];
		return $tanggal.' '.$bulan.' '.$tahun;
	}
}

if ( ! function_exists('bulan'))
{
	function bulan($bln)
	{
		switch ($bln)
		{
			case 1:
				return "Januari";
				break;
			case 2:
				return "Februari";
				break;
			case 3:
				return "Maret";
				break;
			case 4:
				return "April";
				break;
			case 5:
				return "Mei";
				break;
			case 6:
				return "Juni";
				break;
			case 7:
				return "Juli";
				break;
			case 8:
				return "Agustus";
				break;
			case 9:
				return "September";
				break;
			case 10:
				return "Oktober";
				break;
			case 11:
				return "November";
				break;
			case 12:
				return "Desember";
				break;
		}
	}
}

if ( ! function_exists('nama_hari'))
{
	function nama_hari($tanggal)
	{
		$ubah = gmdate($tanggal, time()+60*60*8);
		$pecah = explode("-",$ubah);
		$tgl = $pecah[2];
		$bln = $pecah[1];
		$thn = $pecah[0];

		$nama = date("l", mktime(0,0,0,$bln,$tgl,$thn));
		$nama_hari = "";
		if($nama=="Sunday") {$nama_hari="Minggu";}
		else if($nama=="Monday") {$nama_hari="Senin";}
		else if($nama=="Tuesday") {$nama_hari="Selasa";}
		else if($nama=="Wednesday") {$nama_hari="Rabu";}
		else if($nama=="Thursday") {$nama_hari="Kamis";}
		else if($nama=="Friday") {$nama_hari="Jumat";}
		else if($nama=="Saturday") {$nama_hari="Sabtu";}
		return $nama_hari;
	}
}

					function dateDiff($dformat, $endDate, $beginDate){
						$date_parts1=explode($dformat, $beginDate);
						$date_parts2=explode($dformat, $endDate);
						$start_date=gregoriantojd($date_parts1[1],$date_parts1[0],$date_parts1[2]);
						$end_date=gregoriantojd($date_parts2[1],$date_parts2[0], $date_parts2[2]);
						return ($end_date)-($start_date);
					}
					 function ubahformatTgl($tanggal) {
							$pisah = explode('-',$tanggal);
							$urutan = array($pisah[2],$pisah[1],$pisah[0]);
							$satukan = implode('/',$urutan);
							return $satukan;
						}
						


?>