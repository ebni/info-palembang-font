<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Jadwal_model extends CI_Model {
		public function jadwal()
		{
	        $this->db->order_by('tanggal', 'DESC');
	        return $this->db->get('jadwal')->result();
		}
	}
?>