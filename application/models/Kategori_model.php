<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Kategori_model extends CI_Model {
		public function layanan_count($kd)
		{
			$sql 		= "Select COUNT(kd_layanan) as jml from layanan where kd_kategori = '$kd' ";			
			return $this->db->query($sql)->row();
		}

		public function dinas()
		{
	        $this->db->order_by('kd_kategori', 'ASC');
	        $this->db->where('kd_parent', '0');
	        return $this->db->get('kategori')->result();
		}

		public function dinas_where_parent($kd)
		{
	        $this->db->order_by('kd_kategori', 'ASC');
	        $this->db->where('kd_parent', $kd);
	        return $this->db->get('kategori')->result();
		}

		public function dinas_join_info_dashboard()
		{
	        $this->db->join('informasi_kategori', 'informasi_kategori.kd_kategori=kategori.kd_kategori', 'left');
	        $this->db->order_by('kategori.nama_kategori', 'ASC');
	        $this->db->where('kategori.kd_parent', '0');
	        $this->db->limit('4','0');
	        return $this->db->get('kategori')->result();
		}

		public function dinas_id($id)
		{
	        $this->db->join('kategori', 'informasi_kategori.kd_kategori=kategori.kd_kategori', 'left');
	        $this->db->where('kategori.kd_kategori', $id);
	        return $this->db->get('informasi_kategori')->row();
		}

		public function layanan($id)
		{
	        $this->db->join('kategori', 'layanan.kd_kategori=kategori.kd_kategori', 'left');
			$this->db->where('layanan.kd_kategori', $id);
	        $this->db->order_by('layanan.kd_layanan', 'ASC');
	        return $this->db->get('layanan')->result();
		}

		public function layanan_detail($id)
		{
	        $this->db->join('kategori', 'layanan.kd_kategori=kategori.kd_kategori', 'left');
			$this->db->where('layanan.kd_layanan', $id);
	        $this->db->order_by('layanan.kd_layanan', 'ASC');
	        return $this->db->get('layanan')->result();
		}

		public function sub_dinas($id)
		{
			$this->db->where('kd_parent', $id);
	        $this->db->order_by('kd_kategori', 'ASC');
	        return $this->db->get('kategori')->result();
		}

		public function count_subdinas($id)
		{
			$this->db->where('kd_parent', $id);
			$this->db->from('kategori');
	        return $this->db->count_all_results();
		}
	}
?>