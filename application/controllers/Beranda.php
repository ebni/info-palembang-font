<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Beranda extends CI_Controller
	{
		public function __construct()
		{
		parent::__construct();
			$this->load->helper('url');
			$this->load->model('Kategori_model');
			$this->halaman = 'beranda';
		}
		
		public function index()
		{
			$kategori = $this->Kategori_model->dinas_join_info_dashboard();
			$data = array(
				'kategori' => $kategori );
			$this->load->view('template/header');
			$this->load->view('index', $data);
			$this->load->view('template/footer');
		}
				
	}
?>