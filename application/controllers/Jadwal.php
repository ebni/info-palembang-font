<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Jadwal extends CI_Controller
	{
		public function __construct()
		{
		parent::__construct();
			$this->load->helper('url');
			$this->load->model('Jadwal_model');
		}
		
		public function index()
		{
			$jadwal = $this->Jadwal_model->jadwal();
			$data = array(
				'jadwal' => $jadwal );
			$this->load->view('template/header');
			$this->load->view('jadwal', $data);
			$this->load->view('template/footer');
		}
	}
?>