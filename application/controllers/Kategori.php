<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Kategori extends CI_Controller
	{
		public function __construct()
		{
		parent::__construct();
			$this->load->model('Kategori_model');
			$this->load->helper('url');
        	$this->load->library(array('googlemaps'));
		}
		
		public function index($id = null, $kd_parent = null)
		{
			$kd	= $id;
			$kd_parent = $kd_parent;
			$ceklayanan = $this->Kategori_model->layanan_count($kd);
			$cek		= $ceklayanan->jml;
			if($cek == 0){
				$kategori = $this->Kategori_model->dinas_where_parent($kd);
				$data = array(
					'kategori' => $kategori,
					'kd_parent' => $kd_parent
 				);
			$this->load->view('template/header');
			$this->load->view('layanan', $data);
			$this->load->view('template/footer');

			}else if($cek > 0){
				$layanan = $this->Kategori_model->layanan($kd);
				$kategori = $this->Kategori_model->dinas_id($kd_parent);

				$config['center']=$kategori->koordinat;
	            $config['apiKey']='AIzaSyDwecVrTRpLKnnzAmfFQ6MCgxafnqMCm-Y';
	            $config['zoom']=12;
	            $config['map_height']="400px";
	            $this->googlemaps->initialize($config);

                $marker=array();
                //$marker['icon']=$image;
                $marker['position']=$kategori->koordinat;
                // $marker['infowindow_content']="<center><img src='http://admin.montir-ku.com/assets/montir-ku/img/petugas/".$coordinate->Photo_Petugas."'  width='100' height='80'></center> <b>".$coordinate->Nama_Petugas." - #".$coordinate->Id_Petugas."</b><br>Tlp.+".$coordinate->No_Hp_Petugas.$transaksi;
                // $marker['title']=$status_petugas;

                $this->googlemaps->add_marker($marker);
                $lokasi = $this->address_maps($kategori->koordinat);

				$data = array(
					'layanan' => $layanan,
					'kd_parent' => $kd_parent,
				'alamat' => $kategori->alamat,
				'nama_kategori' => $kategori->nama_kategori,
				'info_tambahan' => $kategori->info_tambahan,
				'email' => $kategori->email,
				'kontak' => $kategori->kontak,
        		'map' => $this->googlemaps->create_map(),
        		'lokasi' => $lokasi
 				);
			$this->load->view('template/header');
			$this->load->view('daftar_layanan', $data);
			$this->load->view('template/footer');
			}

			
		}
		
    public function address_maps($latlong)
    {
        $url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=$latlong&sensor=false";
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_ENCODING, "");
        $curlData = curl_exec($curl);
        curl_close($curl);
        
        //$address = json_decode($curlData);
        //return print_r($address);
        $json_decode = json_decode($curlData);
        
        if(isset($json_decode->results[0])) {
                    return $json_decode->results[0]->formatted_address."<br><a href='https://maps.google.com/maps?q=".$latlong."&t=k' target='_blank'>".$latlong."</a>";
            
        }else{
                return "<a href='https://maps.google.com/maps?q=".$latlong."&t=k' target='_blank'>".$latlong."</a>";
        }
    }
				
	}
?>